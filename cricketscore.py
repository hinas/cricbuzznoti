import time
import pync
from pycricbuzz import Cricbuzz

def liveScore() :
    cric = Cricbuzz()
    matches = cric.matches()
    matches = filter(None,matches)
    message = "No matches live!"

    for match in matches :
        if 'mchstate' in match :
            if match['mchstate'] == 'inprogress' :
                matchid = match['id']
                livescore = cric.livescore(matchid)
                try :
                    ms = livescore['batting']['score'][0]
                    bat1 = livescore['batting']['batsman'][0]
                    bat1score = bat1['name'] + ":" + bat1['runs'] + "(" + bat1['balls'] + ")"
                    bat2 = livescore['batting']['batsman'][1]
                    bat2score = bat2['name'] + ":" + bat2['runs']+ "(" + bat2['balls'] + ")"
                except :
                    bat1score = ""
                    bat2score = ""
                message = "Score: " + livescore['batting']['team'] + " " + ms['runs'] + '/' + ms['wickets'] + " (" + ms['overs'] + ")" + "\n" + bat1score + "  " + bat2score
                pync.notify(message, title=match['srs'] + " " + match['type'], group=match['id'], appIcon="icon.png", open="https://www.cricbuzz.com")
    time.sleep(120)


while True :
  liveScore()
